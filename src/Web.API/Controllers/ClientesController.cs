using Application.Clientes.GetAll;

namespace Web.API.Controllers;

[Route("clientes")]
public class Clientes : ApiController
{
    private readonly ISender _mediator;

    public Clientes(ISender mediator)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }

    //Obtiene listado de clientes
    [HttpGet]
    public async Task<IActionResult> GetAll()
    {
        var result = await _mediator.Send(new GetAllClientesQuery());

        return result.Match(clientes => Ok(clientes),
                            errors => Problem(errors)
                            );
    }
}