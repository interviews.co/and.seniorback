namespace Domain.ValueObjects;

public partial record Direccion
{
    public Direccion(string pais, string departamento, string ciudad, string codigoPostal,string calle, string carrera )
    {
        Pais = pais;
        Departamento = departamento;
        Ciudad = ciudad;
        CodigoPostal = codigoPostal;
        Calle = calle;
        Carrera = carrera;
    }

    public string Pais { get; init; }
    public string Ciudad { get; init; }
    public string Departamento { get; init; }
    public string Calle { get; init; }
    public string Carrera { get; init; }
    public string CodigoPostal { get; init; }

    public static Direccion? Create(string pais, string departamento, string ciudad, string codigoPostal, string calle, string carrera)
    {
        if (string.IsNullOrEmpty(pais) || string.IsNullOrEmpty(departamento) || string.IsNullOrEmpty(ciudad) || string.IsNullOrEmpty(codigoPostal) || string.IsNullOrEmpty(calle) || string.IsNullOrEmpty(carrera))
        {
            return null;
        }

        return new Direccion(pais, departamento, ciudad, codigoPostal, calle, carrera);
    }
}