using Domain.Clientes;
using Domain.Primitives;

namespace Domain.Clientes;

public sealed class Cliente : AggregateRoot
{
    public Cliente(ClienteId id, string nombres, string apellidos, string sexo, string correo, Telefono telefono, Direccion direccion, bool estado)
    {
        Id = id;
        Nombres = nombres;
        Apellidos = apellidos;
        Correo = correo;
        Sexo = Sexo;
        Telefono = telefono;
        Direccion = direccion;
        Estado = estado;
    }

    public Cliente()
    {

    }

    public ClienteId Id { get; set; }
    public string Nombres { get; set; } = string.Empty;
    public string Apellidos { get; set; } = string.Empty;
    public string NombreCompleto => $"{Nombres} {Apellidos}";
    public string Sexo { get; set; } = string.Empty;
    public string Correo { get; set; } = string.Empty;
    public Telefono Telefono { get; set; }
    public Direccion Direccion { get; set; }
    public bool Estado { get; set; }

    public static Cliente UpdateCliente(Guid id, string nombres, string apellidos, string sexo, string correo, Telefono telefono, Direccion direccion, bool estado)
    {
        return new Cliente(new ClienteId(id), nombres, apellidos, sexo, correo, telefono, direccion, estado);
    }
}