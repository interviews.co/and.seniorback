namespace Domain.ValueObjects;

public partial record Telefono
{
    private const int DefaultLenght = 9;

    public Telefono(string telefono) => Value = telefono;

    public static Telefono? Create(string telefono)
    {
        if(string.IsNullOrEmpty(telefono) || telefono.Length != DefaultLenght)
        {
            return null;
        }

        return new Telefono(telefono);
    }

    public string Value { get; init; }

}