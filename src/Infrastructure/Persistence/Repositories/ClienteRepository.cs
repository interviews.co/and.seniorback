using Domain.ValueObjects;
using Domain.Clientes;

namespace Infrastructure.Persistence.Repositories;

public class ClienteRepository : IClienteRepository
{
    public List<Cliente> _clientes;

    public ClienteRepository()
    {
        _clientes = new List<Cliente>
        {
            new Cliente { Id = new ClienteId(new Guid()),
                Nombres = "Maylcon",
                Apellidos = "Saray Ramirez",
                Sexo = "Masculino",
                Correo = "correofalso1@gmail.com",
                Telefono = new Telefono("3111234567"),
                Direccion = new Direccion("Colombia", "Meta", "Cumaral", "50226", "Calle 15", "Carrea 25 98"),                
                Estado = true
            },
            new Cliente { Id = new ClienteId(new Guid()),
                Nombres = "Leonardo",
                Apellidos = "Yepes",
                Sexo = "Masculino",
                Correo = "correofalso2@hotmail.com",
                Telefono = new Telefono("3207654321"),
                Direccion = new Direccion("Colombia", "Meta", "Villavicencio", "50001", "Calle 123", "Carrea 45 67"),
                Estado = false
            },
            new Cliente { Id = new ClienteId(new Guid()),
                Nombres = "Patricia",
                Apellidos = "Fernandez",
                Sexo = "Femenino",
                Correo = "correofalso3@hotmail.com",
                Telefono = new Telefono("3209874563"),
                Direccion = new Direccion("Colombia", "Cundinamarca", "Bogota", "001001", "Calle 456", "Carrea 7 89"),
                Estado = true
            },
            new Cliente { Id = new ClienteId(new Guid()),
                Nombres = "Beatriz",
                Apellidos = "Pinzon Solano",
                Sexo = "Femenino",
                Correo = "correofalso4@yopmail.com",
                Telefono = new Telefono("3208745962"),
                Direccion = new Direccion("Colombia", "Cundinamarca", "Bogota", "001001", "Calle 78", "Carrea 89 01"),
                Estado = true
            },
            new Cliente { Id = new ClienteId(new Guid()),
                Nombres = "Armando",
                Apellidos = "Paredes",
                Sexo = "Masculino",
                Correo = "correofalso5@yopmail.com",
                Telefono = new Telefono("31245678905"),
                Direccion = new Direccion("Colombia", "Cundinamarca", "Bogota", "001001", "Calle 89", "Carrea 01 23"),
                Estado = true
            },
        };
    }

    public async Task<List<Cliente>> GetAll()
    {

        await Task.Delay(1); 

        return _clientes;
    }
}