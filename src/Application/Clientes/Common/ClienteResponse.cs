namespace Clientes.Common;

public record ClienteResponse(
Guid Id,
string NombreCompleto,
string Sexo,
string Correo,
string Telefono,
bool Estado,
DireccionResponse Direccion
);

public record DireccionResponse(
    string Pais,
    string Departamento,
    string Ciudad,
    string CodigoPostal,
    string Calle,
    string Carrera);