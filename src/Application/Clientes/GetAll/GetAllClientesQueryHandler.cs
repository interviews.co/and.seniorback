using Domain.Clientes;
using Clientes.Common;

namespace Application.Clientes.GetAll;


public sealed class GetAllClientesQueryHandler : IRequestHandler<GetAllClientesQuery, ErrorOr<IReadOnlyList<ClienteResponse>>>
{
    private readonly IClienteRepository _clienteRepository;

    public GetAllClientesQueryHandler(IClienteRepository clienteRepository)
    {
        _clienteRepository = clienteRepository ?? throw new ArgumentNullException(nameof(clienteRepository));
    }

    public async Task<ErrorOr<IReadOnlyList<ClienteResponse>>> Handle(GetAllClientesQuery query, CancellationToken cancellationToken)
    {

        IReadOnlyList<Cliente> clientes = await _clienteRepository.GetAll();

        return clientes.Select(cliente => new ClienteResponse(
                                cliente.Id.Value,
                                cliente.NombreCompleto,
                                cliente.Sexo,
                                cliente.Correo,
                                cliente.Telefono.Value,
                                cliente.Estado,
                                new DireccionResponse (cliente.Direccion.Pais,
                                                       cliente.Direccion.Departamento,
                                                       cliente.Direccion.Ciudad,
                                                       cliente.Direccion.CodigoPostal,
                                                       cliente.Direccion.Calle,
                                                       cliente.Direccion.Carrera)
                        )).ToList();


    }
}