using ErrorOr;

namespace Domain.DomainErrors;

public static partial class Errors
{
    public static class Cliente
    {
        public static Error TelefonoNoValido => 
            Error.Validation("Cliente.Telefono", "Telefono no es valido.");

        public static Error DireccionNoValida => 
            Error.Validation("Cliente.Direccion", "Direcci�n no es valida.");
    }
}