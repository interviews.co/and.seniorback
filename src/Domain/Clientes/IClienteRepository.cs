namespace Domain.Clientes;

public interface IClienteRepository
{
    Task<List<Cliente>> GetAll();
}