﻿using Application.Clientes.GetAll;
using Domain.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Clientes.UnitTests.GetAll
{
    public class GetAllClientesQueryHandlerUnitTests
    {

        private readonly Mock<IClienteRepository> _mockClienteRepository;

        private readonly GetAllClientesQueryHandler _handler;

        public GetAllClientesQueryHandlerUnitTests()
        {
            _mockClienteRepository = new Mock<IClienteRepository>();
            _handler = new GetAllClientesQueryHandler(_mockClienteRepository.Object);
        }

        [Fact]
        public async Task HandlerGetAllClientes_NotResult()
        {

            var result = _handler.Handle(null, default);
            Assert.NotNull(result);
        }

    }
}
